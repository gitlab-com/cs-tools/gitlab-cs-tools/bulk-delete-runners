import argparse
import datetime
import gitlab

def delete_runners(gl, runners, where, stale_date, no_act, limit):
    all_runners = 0
    deleted_runners = 0
    for runner in runners:
        is_stale = False
        contact_date = None

        if limit is not None and deleted_runners >= limit:
            print("Deletion limit reached.")
            break

        if stale_date is not None:
            runner_object = gl.runners.get(runner.id)
            if runner_object.contacted_at is not None:
                contacted = datetime.datetime.strptime(runner_object.contacted_at, '%Y-%m-%dT%H:%M:%S.%fZ')
                contact_date = contacted.date()
                if contact_date < stale_date.date():
                    is_stale = True
            else:
                is_stale = True
        all_runners += 1
        try:
            if stale_date is None:
                if no_act:
                    print("Would delete runner %s:%s" % (runner.id, runner.description))
                    deleted_runners += 1
                else:
                    gl.runners.delete(runner.id, retry_transient_errors=True)
                    print("Deleted runner %s:%s" % (runner.id, runner.description))
                    deleted_runners += 1
            else:
                if is_stale:
                    if no_act:
                        print("Would delete runner %s:%s, contacted at %s" % (runner.id, runner.description, contact_date))
                        deleted_runners += 1
                    else:
                        gl.runners.delete(runner.id , retry_transient_errors=True)
                        print("Deleted runner %s:%s, contacted at %s" % (runner.id, runner.description, contact_date))
                        deleted_runners += 1
        except Exception as e:
            print("Unable to delete %s:%s in %s. Exception: %s" % (runner.id, runner.description, where, str(e)))

    if no_act:
        print("Would delete %s runners found in %s" % (deleted_runners, where))
    else:
        print("Deleted %s of %s runners found in %s" % (deleted_runners, all_runners, where))

parser = argparse.ArgumentParser(description='Delete group runners')
parser.add_argument('token', help='API token able to read the requested group (Owner)')
parser.add_argument('-n', '--no-act', help='Print the runners that would be deleted, but take no action', action='store_true')
parser.add_argument('-g','--group', help='Group IDs to delete runners on', action='append')
parser.add_argument('-p','--project', help='Project IDs to delete runners on', action='append')
parser.add_argument('-l','--limit', help='Only delete [limit] number of runners', type=int, default=None)
parser.add_argument('--tags', help='Optional list of tags of runners to be deleted (comma separated)')
parser.add_argument('--stale_date', help='Oldest date of last contact. Runners with contacted_at before this date will be deleted. YYYY-MM-DD.')
parser.add_argument('--gitlab', help='The URL of the GitLab instance', default="https://gitlab.com")
args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token)

tags = args.tags if args.tags else None

stale_date = None
if args.stale_date:
    print("WARN: Running this with stale_date will result in one additional API call per runner to get their contacted_at date. This may be slow.")
    try:
        stale_date = datetime.datetime.strptime(args.stale_date, '%Y-%m-%d')
        if stale_date > datetime.datetime.now():
            print("ERROR: Stale date may not be in the future")
            exit(1)
    except Exception as e:
        print("ERROR: Not a valid date: %s. Format must be YYYY-MM-DD" % args.stale_date)
        exit(0)

if args.group:
    for group in args.group:
        group = gl.groups.get(group, retry_transient_errors=True)
        if tags:
            runners = group.runners.list(as_list=False, tag_list=tags, type="group_type", retry_transient_errors=True)
        else:
            runners = group.runners.list(as_list=False, type="group_type", retry_transient_errors=True)
        delete_runners(gl, runners, "group " + str(group.id), stale_date, args.no_act, args.limit)

if args.project:
    for project in args.project:
        project = gl.projects.get(project, retry_transient_errors=True)
        if tags:
            runners = project.runners.list(as_list=False, tag_list=tags, type="project_type", retry_transient_errors=True)
        else:
            runners = project.runners.list(as_list=False, type="project_type", retry_transient_errors=True)
        delete_runners(gl, runners, "project " + str(project.id), stale_date, args.no_act, args.limit)

if not args.group and not args.project:
    if tags:
        runners = gl.runners.all(as_list=False, tag_list=tags, type="instance_type", retry_transient_errors=True)
    else:
        runners = gl.runners.all(as_list=False, type="instance_type", retry_transient_errors=True)
    delete_runners(gl, runners, "instance", stale_date, args.no_act, args.limit)